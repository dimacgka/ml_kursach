import logging
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from aiogram.types import ParseMode
from aiogram.utils import executor
from transformers import BertTokenizer, BertForSequenceClassification
import torch
import pdfplumber
import io

# Настройка логирования
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

API_TOKEN = ''

# Инициализация бота и диспетчера
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)
dp.middleware.setup(LoggingMiddleware())

model_name = 'bert-base-uncased'
tokenizer = BertTokenizer.from_pretrained(model_name)
model = BertForSequenceClassification.from_pretrained(model_name)

labels = [
    'application',  # заявка
    'explanation',  # объяснительная
    'report',  # рапорт
    'invoice',  # счет-фактура
    'contract',  # контракт
    'memo',  # служебная записка
    'letter',  # письмо
    'proposal',  # предложение
    'resume',  # резюме
    'receipt',  # квитанция
    'minutes',  # протокол
    'manual',  # руководство
    'policy',  # политика
    'plan',  # план
    'agenda',  # повестка дня
    'note',  # заметка
    'announcement',  # объявление
    'notification'  # уведомление
]


@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    await message.reply("Hi! Send me a PDF document, and I will classify its type.")


@dp.message_handler(content_types=[types.ContentType.DOCUMENT])
async def handle_docs(message: types.Message):
    if message.document.mime_type == 'application/pdf':
        file_info = await bot.get_file(message.document.file_id)
        file = await bot.download_file(file_info.file_path)

        file_content = io.BytesIO(file.read())

        try:
            with pdfplumber.open(file_content) as pdf:
                text = ''
                for page in pdf.pages:
                    page_text = page.extract_text()
                    if page_text:
                        text += page_text + '\n'

                if not text:
                    await message.reply("Sorry, I couldn't extract any text from the PDF.")
                    return

                # Токенизация текста и классификация с использованием модели BERT
                inputs = tokenizer(text, return_tensors="pt", padding=True, truncation=True)
                with torch.no_grad():
                    outputs = model(**inputs)
                predicted_label_idx = torch.argmax(outputs.logits, dim=1).item()
                predicted_label = labels[predicted_label_idx]
                confidence = torch.softmax(outputs.logits, dim=1).max().item()

                response = f"Document type: {predicted_label}\nConfidence: {confidence:.2f}"
                await message.reply(response)

        except Exception as e:
            await message.reply(f"An error occurred: {e}")
    else:
        await message.reply("Please send a PDF document.")


async def on_startup(dispatcher):
    logger.info('Bot is starting...')


if __name__ == '__main__':
    executor.start_polling(dp, on_startup=on_startup)
